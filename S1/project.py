import pytest
import copy
import graph


def debug(installation, arche):
    a = copie_grille(arche)
    if realise_installation(installation, arche):
        print("OK")
        affiche_grille(arche)
    else:
        print("KO")


##
## Position et Vecteurs
##


def position(ligne, colonne):
    return (ligne, colonne)


def vecteur(ligne, colonne):
    return (ligne, colonne)


def ligne_de(position):
    return position[0]

def colonne_de(position):
    return position[1]


def addition2(pos1, v1):
    return position(ligne=ligne_de(pos1) + ligne_de(v1), colonne=colonne_de(pos1) + colonne_de(v1))


def test_addition2():
    assert addition2(position(ligne=3, colonne=5), vecteur(ligne=1, colonne=0)) == position(ligne=4, colonne=5)
    assert addition2(position(ligne=3, colonne=5), vecteur(ligne=1, colonne=1)) == position(ligne=4, colonne=6)


def vosins_de(position):
    return [addition2(position, v) for v in [(0, 1), (0, -1), (1, 0), (-1, 0)]]


##
## Arche et Grille
##


def arche_vide():
    return [
        ["0","0","0","0","0","0","0","0","0"],
        ["0","0",".",".",".",".",".","0","0"],
        ["0",".",".",".",".",".",".",".","0"],
        ["0",".",".",".",".",".",".",".","0"],
        ["0","0",".",".",".",".",".","0","0"],
        ["0","0","0","0","0","0","0","0","0"]]


def largeur_grille(grille):
    return len(grille[0])


def hauteur_grille(grille):
    return len(grille)


def est_dans_grille(position, grille):
    if 0 <= ligne_de(position) < hauteur_grille(grille):
        if 0 <= colonne_de(position) < largeur_grille(grille):
            return True
    return False


def test_est_dans_grille():
    assert est_dans_grille(position(ligne=0, colonne=9),arche_vide()) == False
    assert est_dans_grille(position(ligne=0, colonne=8),arche_vide()) == True


def case(position, grille):
    if est_dans_grille(position, grille):
        return grille[ligne_de(position)][colonne_de(position)]
    return "0"


def change_case(position, grille, contenu):
    if est_dans_grille(position, grille):
        grille[ligne_de(position)][colonne_de(position)] = contenu


def effface_case(position, grille):
    change_case(position, grille, ".")


def case_libre(position, grille):
    return case(position, grille) == "."


def affiche_grille(grille):
    for line in grille:
        for cell in line:
            print(cell.ljust(2, " "), end='')
        print("")


def copie_grille(grille):
    return copy.deepcopy(grille)


##
## Animal
##


def animal(race, sexe, vecteurs):
    return (race, sexe, vecteurs)


def race_animal(animal):
    return animal[0]


def sexe_animal(animal):
    return animal[1]


def vecteurs_de_animal(animal):
    return animal[2]


def identification_animal(animal):
    return race_animal(animal) + str(sexe_animal(animal))


def place_occupe_animal(position, animal):
    return [addition2(position, v) for v in vecteurs_de_animal(animal)]


def test_place_occupe_animal():
    assert place_occupe_animal(position(1, 3), Lion2) == [position(1, 3), position(1, 4), position(2, 3)]


def sont_voisins(animal1, position1, animal2, position2):
    place_animal2 = place_occupe_animal(position2, animal2)
    for p in place_occupe_animal(position1, animal1):
        for voisin in vosins_de(p):
            if voisin in place_animal2:
                return True
    return False


def test_sont_voisins():
    assert sont_voisins(Lion1, position(1, 2), Lion2, position(3, 2)) == False
    assert sont_voisins(Lion1, position(1, 2), Lion2, position(2, 2)) == True


Lion1 = animal('L', 1, [(0, 0), (0, 1)])
Lion2 = animal('L', 2, [(0, 0), (0, 1), (1, 0)])
Girafe1 = animal('G', 1, [(0, 0), (0, 1), (-1, 1)])
Girafe2 = animal('G', 2, [(0, 0), (1, 0)])
Hippo1 = animal('H', 1, [(0, 0), (0, 1)])
Hippo2 = animal('H', 2, [(0, 0), (0, 1), (0, 2)])
Zebre1 = animal('Z', 1, [(0, 0), (0, 1)])
Zebre2 = animal('Z', 2, [(0, 0), (1, 0)])
Elephant1 = animal('E', 1, [(0, 0), (0, 1)])
Elephant2 = animal('E', 2, [(0, 0), (0, 1), (1, 1)])


Couples = [(Lion1, Lion2), (Girafe1, Girafe2), (Hippo1, Hippo2), (Zebre1, Zebre2), (Elephant1, Elephant2)]


##
## Installation
##


def toutes_les_positions(installation):
    return dict((identification_animal(animal), position) for animal, position in installation)


def couples_sont_voisins(installation, couples):
    positions = toutes_les_positions(installation)
    for un, deux in couples:
        if identification_animal(un) in positions and identification_animal(deux) in positions:
            if not sont_voisins(un, positions[identification_animal(un)], deux, positions[identification_animal(deux)]):
                return False
    return True


def test_couples_sont_voisins():
    assert couples_sont_voisins([(Lion1, position(1, 2)), (Lion2, position(2, 2))], Couples) == True
    assert couples_sont_voisins([(Lion1, position(1, 2)), (Elephant1, position(3, 4))], Couples) == True


##
## Logique
##


def place_libre(position, grille, animal):
    return all(case_libre(p, grille) for p in place_occupe_animal(position, animal))


def test_place_libre():
    assert place_libre(position(2, 3), arche_vide(), Lion1) == True
    assert place_libre(position(1, 1), arche_vide(), Lion1) == False
    assert place_libre(position(10, 10), arche_vide(), Lion1) == False


def installe_animal(position, grille, animal):
    if place_libre(position, grille, animal):
        for p in place_occupe_animal(position, animal):
            change_case(p, grille, identification_animal(animal))

def enleve_animal(position, grille, animal):
    for p in place_occupe_animal(position, animal):
        effface_case(p, grille)


def realise_installation(installation, grille):
    for animal, position in installation:
        if place_libre(position, grille, animal):
            installe_animal(position, grille, animal)
        else:
            return False
    return True



def verifie_installation(arche, installation, couples):
    if couples_sont_voisins(installation, couples):
        if realise_installation(installation, copie_grille(arche)):
            return True
    return False


def polop(arche, initial, restants, couples, solutions):
    if restants == []:
        solutions.append(initial)
        return True
    suivant = restants[0]
    for ligne in range(hauteur_grille(arche)):
        for colonne in range(largeur_grille(arche)):
            installation = initial + [(suivant, position(ligne, colonne))]
            if verifie_installation(arche, installation, couples):
                if polop(arche, installation, restants[1:], couples, solutions):
                    return True
    return False


##
##affichage
##

Colors = {
    "L1": "yellow",
    "L2": "light yellow",
    "G1": "green",
    "G2": "light green",
    "E1": "magenta",
    "E2": "purple",
    "Z1": "red",
    "Z2": "firebrick",
    "H1": "blue",
    "H2": "light blue",
    "0": "burlywood"}


def color_for(case):
    if case in Colors:
        return Colors[case]
    return "white"

def test_color_for():
    assert color_for("L1") == "yellow"
    assert color_for("") == "white"


def draw(larg , haut, cote, grille):
    for i in range(largeur_grille(grille)):
        for j in range(hauteur_grille(grille)):
            draw_square(
                *to_square(i, j, larg, haut, cote),
                color_for(case(position(ligne=j, colonne=i), grille)))


def draw_square(tlx, tly, brx, bry, coul):
    for x in range(tlx, brx):
        for y in range(tly, bry):
            graph.plot(y, x, coul)


def to_square(col, row, larg, haut, cote):
    return (
        col * cote,
        row * cote,
        min((col + 1) * cote, larg),
        min((row + 1) * cote, haut)
    )



if __name__ == "__main__":
    arche = arche_vide()

    initial = [(Elephant1, (2, 4))]

    restants = [Lion1, Lion2, Girafe1, Girafe2, Hippo1, Hippo2, Zebre1, Zebre2, Elephant2]

    solutions = []
    if polop(arche, initial, restants, Couples, solutions):
        print("OK")
        if realise_installation(solutions[0], arche):
            affiche_grille(arche)
            graph.ouvre_fenetre(800,800)
            draw(800 , 800, 50, arche)
            graph.attend_fenetre()


    else:
        print("KO")
