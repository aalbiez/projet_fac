# -*- coding: utf-8 -*-
from __future__ import annotations

import pytest
from geometry import Position
from arche import new_arche


def test_arche_contient():
    assert new_arche().contient(Position(0, 9)) == False
    assert new_arche().contient(Position(0, 8)) == True
