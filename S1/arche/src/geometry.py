# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass


@dataclass(frozen = True, order = True)
class Vecteur:
    deplacement_ligne: int
    deplacement_colonne: int


@dataclass(frozen = True, order = True)
class Position:
    ligne: int
    colonne: int

    def voisins(self):
        return [self.addition(v) for v in [Vecteur(0, 1), Vecteur(0, -1), Vecteur(1, 0), Vecteur(-1, 0)]]

    def addition(self, vecteur):
        return Position(
            self.ligne + vecteur.deplacement_ligne,
            self.colonne + vecteur.deplacement_colonne)
