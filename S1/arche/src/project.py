# -*- coding: utf-8 -*-
from __future__ import annotations

from typing import Text, List
from dataclasses import dataclass

import pytest
import copy
import graph
from geometry import Vecteur, Position
from animal import *
from arche import new_arche


def debug(installation, arche):
    a = arche.copie()
    if realise_installation(installation, arche):
        print("OK")
        arche.affiche()
    else:
        print("KO")


##
## Installation
##


def toutes_les_positions(installation):
    return dict((animal.identification(), position) for animal, position in installation)


def couples_sont_voisins(installation, couples):
    positions = toutes_les_positions(installation)
    for un, deux in couples:
        if un.identification() in positions and deux.identification() in positions:
            if not sont_voisins(un, positions[un.identification()], deux, positions[deux.identification()]):
                return False
    return True


def test_couples_sont_voisins():
    assert couples_sont_voisins([(Lion1, Position(1, 2)), (Lion2, Position(2, 2))], Couples) == True
    assert couples_sont_voisins([(Lion1, Position(1, 2)), (Elephant1, Position(3, 4))], Couples) == True


##
## Logique
##


def place_libre(position, grille, animal):
    return all(grille.case_libre(p) for p in animal.place_occupe(position))


def test_place_libre():
    assert place_libre(Position(2, 3), new_arche(), Lion1) == True
    assert place_libre(Position(1, 1), new_arche(), Lion1) == False
    assert place_libre(Position(10, 10), new_arche(), Lion1) == False


def installe_animal(position, grille, animal):
    if place_libre(position, grille, animal):
        for p in animal.place_occupe(position):
            grille.change_case(p, animal.identification())

def enleve_animal(position, grille, animal):
    for p in animal.place_occupe(position):
        grille.effface_case(p)


def realise_installation(installation, grille):
    for animal, position in installation:
        if place_libre(position, grille, animal):
            installe_animal(position, grille, animal)
        else:
            return False
    return True



def verifie_installation(arche, installation, couples):
    if couples_sont_voisins(installation, couples):
        if realise_installation(installation, arche.copie()):
            return True
    return False


def polop(arche, initial, restants, couples, solutions):
    if restants == []:
        solutions.append(initial)
        return True
    suivant = restants[0]
    for ligne in range(arche.hauteur()):
        for colonne in range(arche.largeur()):
            installation = initial + [(suivant, Position(ligne, colonne))]
            if verifie_installation(arche, installation, couples):
                if polop(arche, installation, restants[1:], couples, solutions):
                    return True
    return False


##
##affichage
##

Colors = {
    "L1": "yellow",
    "L2": "light yellow",
    "G1": "green",
    "G2": "light green",
    "E1": "magenta",
    "E2": "purple",
    "Z1": "red",
    "Z2": "firebrick",
    "H1": "blue",
    "H2": "light blue",
    "0": "burlywood"}


def color_for(case):
    if case in Colors:
        return Colors[case]
    return "white"

def test_color_for():
    assert color_for("L1") == "yellow"
    assert color_for("") == "white"


def draw(larg , haut, cote, grille):
    for i in range(grille.largeur()):
        for j in range(grille.hauteur()):
            draw_square(
                *to_square(i, j, larg, haut, cote),
                color_for(grille.case(Position(j, i))))


def draw_square(tlx, tly, brx, bry, coul):
    for x in range(tlx, brx):
        for y in range(tly, bry):
            graph.plot(y, x, coul)


def to_square(col, row, larg, haut, cote):
    return (
        col * cote,
        row * cote,
        min((col + 1) * cote, larg),
        min((row + 1) * cote, haut)
    )



if __name__ == "__main__":
    arche = new_arche()

    initial = [(Elephant1, (2, 4))]

    restants = [Lion1, Lion2, Girafe1, Girafe2, Hippo1, Hippo2, Zebre1, Zebre2, Elephant2]

    solutions = []
    if polop(arche, initial, restants, Couples, solutions):
        print("OK")
        if realise_installation(solutions[0], arche):
            arche.affiche()
            graph.ouvre_fenetre(800,800)
            draw(800 , 800, 50, arche)
            graph.attend_fenetre()


    else:
        print("KO")
